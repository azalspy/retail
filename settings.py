# -*- coding: utf-8 -*-

from azalinc.spyd.settings import *

BOT_NAME = 'retail'

SPIDER_MODULES = ['azalspy.retail.spiders']
NEWSPIDER_MODULE = 'azalspy.retail.spiders'

#FEED_URI = "sftp://user:password@some.server:/some/path/to/a/file"

#FEED_STORAGES = {
#    'sftp': "scrapy_feedexporter_sftp.SFTPFeedStorage",
#    'xlsx': "scrapy_xlsx.XlsxItemExporter",
#}

SPIDER_MIDDLEWARES = {
#    'retail.middlewares.retailSpiderMiddleware': 543,
} # https://docs.scrapy.org/en/latest/topics/spider-middleware.html

DOWNLOADER_MIDDLEWARES = {
#    'retail.middlewares.retailDownloaderMiddleware': 543,
#    'scrapy_botproxy.BotProxyMiddleware': 100,
} # https://docs.scrapy.org/en/latest/topics/downloader-middleware.html

EXTENSIONS = {
#    'scrapy.extensions.telnet.TelnetConsole': None,
    #'scrapyslackbot.extensions.SlackBot': 500,
} # https://docs.scrapy.org/en/latest/topics/extensions.html

ITEM_PIPELINES = {
#    'azalinc.spyd.pipeline.MySQLPipeline': 300,
    'azalinc.spyd.pipeline.CoverPipeline': 300,
} # https://docs.scrapy.org/en/latest/topics/item-pipeline.html

